all: pedersen scalarmul varscalarmul verifenc ratchetcommit

LIBSNARK=libsnark

DEFINES=-UBINARY_OUTPUT -DBN_SUPPORT_SNARK=1 -DCURVE_BN128 -UMONTGOMERY_OUTPUT -DUSE_ASM
CXXFLAGS=$(DEFINES) -I$(LIBSNARK) -I$(LIBSNARK)/depends/libfqfft -I$(LIBSNARK)/depends/libff -std=c++11 -Wall -Wextra -Wfatal-errors -pthread -ggdb3 -O2 -march=native -mtune=native -O2 -g -DNDEBUG
LDFLAGS=-L$(LIBSNARK)/build -L$(LIBSNARK)/build/libsnark -L$(LIBSNARK)/build/depends -L$(LIBSNARK)/build/depends/libff/libff -lsnark -lff -lzm -lgmp -lgmpxx -lprocps

pedersen: pedersen.cpp ecgadget.hpp libsnark_headers.hpp.gch pedersen.hpp.gch
	g++ $(CXXFLAGS) -o pedersen pedersen.cpp $(LDFLAGS)

scalarmul: scalarmul.cpp ecgadget.hpp libsnark_headers.hpp.gch scalarmul.hpp.gch
	g++ $(CXXFLAGS) -o scalarmul scalarmul.cpp $(LDFLAGS)

varscalarmul: varscalarmul.cpp ecgadget.hpp libsnark_headers.hpp.gch scalarmul.hpp.gch
	g++ $(CXXFLAGS) -o varscalarmul varscalarmul.cpp $(LDFLAGS)

verifenc: verifenc.cpp ecgadget.hpp libsnark_headers.hpp.gch scalarmul.hpp.gch
	g++ $(CXXFLAGS) -o verifenc verifenc.cpp $(LDFLAGS)

ratchetcommit: ratchetcommit.cpp ecgadget.hpp libsnark_headers.hpp.gch scalarmul.hpp.gch
	g++ $(CXXFLAGS) -o ratchetcommit ratchetcommit.cpp $(LDFLAGS)

libsnark_headers.hpp.gch: libsnark_headers.hpp
	g++ $(CXXFLAGS) $<

pedersen.hpp.gch: pedersen.hpp
	g++ $(CXXFLAGS) $<

scalarmul.hpp.gch: scalarmul.hpp
	g++ $(CXXFLAGS) $<
